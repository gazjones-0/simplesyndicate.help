﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="f95d2b6e-35c8-4d01-ac0f-81e8bfcf593d" revisionNumber="1">
	<developerConceptualDocument xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5" xmlns:xlink="http://www.w3.org/1999/xlink">
		<introduction>
			<para>The topics in this section describe the various changes made over the life of the SimpleSyndicate.Testing.Mvc project.</para>
		</introduction>

		<section>
			<title>Version 1.0.16 was released on 6th January 2017.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (AutoMapper 4.1.1, EntityFramework 6.1.3, Microsoft.AspNet.Identity.Core 2.2.1, Microsoft.AspNet.Identity.EntityFramework 2.2.1, Microsoft.AspNet.Identity.Owin 2.2.1, Microsoft.Owin 3.0.1, Microsoft.Owin.Host.SystemWeb 3.0.1, Microsoft.Owin.Hosting 3.0.1, Microsoft.Owin.Security.Cookies 3.0.1, Microsoft.Owin.Security.Facebook 3.0.1, Microsoft.Owin.Security.Google 3.0.1, Microsoft.Owin.Security.MicrosoftAccount 3.0.1, Microsoft.Owin.Security.OAuth 3.0.1, Microsoft.Owin.Security.Twitter 3.0.1, Microsoft.Owin.Security. 3.0.1, Microsoft.Owin.Testing 3.0.1, Newtonsoft.Json 8.0.3, SimpleInjector 3.1.2, SimpleInjector.Integration.Web 3.1.2, SimpleInjector.Integration.Web.Mvc 3.1.2, SimpleSyndicate 1.0.38, SimpleSyndicate.Mvc 2.0.1, SimpleSyndicate.Testing 1.0.19).
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.15 was released on 10th February 2015.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (AutoMapper 3.3.1, Microsoft.AspNet.Mvc 5.2.3, Microsoft.AspNet.Razor 3.2.3, Microsoft.AspNet.WebApi.Client 5.2.3, Microsoft.AspNet.WebApi.Core 5.2.3, Microsoft.AspNet.WebPages 3.2.3, Microsoft.Bcl 1.1.9, Microsoft.Bcl.Build 1.0.14, Microsoft.Net.Http 2.2.22, SimpleInjector 2.7.1, SimpleInjector.Integration.Web 2.71., SimpleInjector.Integration.Web.Mvc 2.71, SimpleSyndicate 1.0.23, SimpleSyndicate.Mvc 1.0.11, SimpleSyndicate.Testing 1.0.13).
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.14 was released on 1st January 2015.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (AutoMapper 3.3.0, Entity Framework 6.1.2, MvcCodeRouting 1.3.0, Newtonsoft.Json 6.0.7, SimpleInjector 2.6.1, SimpleInjector.Integration.Web 2.6.1, SimpleInjector.Integration.Web.Mvc 2.6.1, SimpleSyndicate 1.0.22, SimpleSyndicate.Mvc 1.0.9, SimpleSyndicate.Testing 1.0.12).
						</para>
					</listItem>
					<listItem>
						<para>
							Minor XML comment fixes.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.13 was released on 19th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Fixed issued with MvcAssert.AllowsHttpGet and MvcAssert.AllowsHttpPost not using correct attribute for Web API.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.12 was released on 19th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added HttpResponseMessage overloads for MvcAssert.AllowsHttpGet and MvcAssert.AllowsHttpPost.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.11 was released on 18th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added missing XML comments in Owin namespace and MvcAssert.GetAttribute.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.10 was released on 18th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added AllowsHttpGet, GetAttribute, HasResponseType, HasRoute and HasResponseType to MvcAssert.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.9 was released on 18th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added string overloads to HasDisplayFormat and HasDisplayName in MvcAssert.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.8 was released on 18th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Fixed dependency issue due to build against pre-release version of SimpleSyndicate.Mvc.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.7 was released on 18th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Resolved code analysis warnings.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.5 was released on 18th November 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added DisplayFormatEquals to MvcAssert.</para>
					</listItem>
					<listItem>
						<para>Added HasDisplayFormat to MvcAssert.</para>
					</listItem>
					<listItem>
						<para>Added HasDisplayName to MvcAssert.</para>
					</listItem>
					<listItem>
						<para>Added HasRegularExpressionErrorMessage and HasRegularExpressionPattern to MvcAssert.</para>
					</listItem>
					<listItem>
						<para>Added HasRequiredErrorMessage to MvcAssert.</para>
					</listItem>
					<listItem>
						<para>Added IsNullOrEmty, IsNullOrWhitespace, IsNotNullOrEmpty, IsNotNullOrWhitespace to MvcAssert.</para>
					</listItem>
					<listItem>
						<para>Added MaxLengthIsValidated, MinLengthIsValidated to MvcAssert.</para>
					</listItem>
					<listItem>
						<para>Refactored MvcAssert into a set of partial classes.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.4 was released on 7th October 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Updated NuGet packages (SimpleSyndicate 1.0.18, SimpleSyndicate.Mvc 1.0.9, SimpleSyndicate.Testing 1.0.11).</para>
					</listItem>
					<listItem>
						<para>Updated online documentation to include SimpleSyndicate, SimpleSyndicate.Mvc and SimpleSyndicate.Testing getting started documentation.</para>
					</listItem>
					<listItem>
						<para>Resolved all code analysis warnings.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.3 was released on 29th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Updated NuGet packages (Asp.Net Identity Core 2.1.0, Asp.Net Identity Entity Framework 2.1.0, Asp.Net Identity Owin 2.1.0, SimpleSyndicate 1.0.17, SimpleSyndicate.Mvc 1.0.8, SimpleSyndicate.Testing 1.0.10).</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.2 was released on 19th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added further unit tests.</para>
					</listItem>
					<listItem>
						<para>Extracted factory methods from TestData\AspNetContainerFactory and TestData\HttpContextContainerFactory to their own classes so they're more intuitive to use.</para>
					</listItem>
					<listItem>
						<para>Added additional overload to several extension methods that are overloaded with a generic and inferred method to take the System.Type parameter to make them harder to use incorrectly.</para>
					</listItem>
					<listItem>
						<para>Fixed bug in GenericController.HasConfirmationMessage extension method.</para>
					</listItem>
					<listItem>
						<para>Minor XML comment fixes.</para>
					</listItem>
					<listItem>
						<para>Updated NuGet package (SimpleSyndicate.Mvc 1.0.6).</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.1 was released on 19th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added initial set of unit tests.</para>
					</listItem>
					<listItem>
						<para>Fixed bug in AspNetContainerFactory.Create.</para>
					</listItem>
					<listItem>
						<para>Fixed bug in RedirectToRouteResult.ControllerIs extension method.</para>
					</listItem>
					<listItem>
						<para>Resolved code analysis warnings.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.0 was released on 18th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Initial release.</para>
					</listItem>
				</list>
			</content>
		</section>

		<relatedTopics>
			<link xlink:href="f50bb335-d9af-4fb1-8715-129f4be65848" />
		</relatedTopics>
	</developerConceptualDocument>
</topic>
