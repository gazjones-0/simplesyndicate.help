﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="a1594a67-cb02-4264-aafb-c31469a88ed4" revisionNumber="1">
	<developerConceptualDocument xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5" xmlns:xlink="http://www.w3.org/1999/xlink">
		<introduction>
			<para>The topics in this section describe the various changes made over the life of the SimpleSyndicate.Mvc project.</para>
		</introduction>

		<section>
			<title>Version 2.0.1 was released on 6th January 2017.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (Newtonsoft.Json 8.03, SimpleInjector 3.1.2, SimpleInjector.Integration.Web 3.1.2, SimpleInjector.Integration.Web.Mvc 3.1.2, SimpleSyndicate 1.0.37, SimpleSyndicate.Testing 1.0.19).
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 2.0.0 was released on 26th November 2015.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Full support for AspNet.Identity 2.2 using SimpleInjector for DI rather than Owin.
						</para>
						<para>
							Upgraded from AutoMapper 3 to AutoMapper 4.
						</para>
						<para>
							Upgraded from SimpleInjector 2 to SimpleInjector 3.
						</para>
						<para>
							Updated NuGet packages (AutoMapper 4.1.1, EntityFramework 6.1.3, Microsoft.AspNet.Identity.Core 2.2.1, Microsoft.AspNet.Identity.EntityFramework 2.2.1, Microsoft.AspNet.Identity.Owin 2.2.1,
							Microsoft.Owin 3.0.1, Microsoft.Owin.Host.SystemWeb 3.0.1, Microsoft.Owin.Security.Cookies 3.0.1, Microsoft.Owin.Security.Facebook 3.0.1, Microsoft.Owin.Security.Google 3.0.1,
							Microsoft.Owin.Security.MicrosoftAccount 3.0.1, Microsoft.Owin.Security.OAuth 3.0.1, Microsoft.Owin.Security.Twitter 3.0.1, Newtonsoft.Json 7.0.1,
							SimpleInjector 3.1.1, SimpleInjector.Integration.Web 3.1.1, SimpleInjector.Integration.Web.Mvc 3.1.1, SimpleSyndicate 1.0.27, SimpleSyndicate.Testing 1.0.18).
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.12 was released on 16th February 2015.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Fixed Simple Injector setup not registering MVC controllers in calling assembly.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.11 was released on 10th February 2015.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (AutoMapper 3.3.1, Microsoft.AspNet.Mvc 5.2.3, Microsoft.AspNet.Razor 3.2.3, Microsoft.AspNet.WebPages 3.2.3, SimpleInjector 2.7.1, SimpleInjector.Integration.Web 2.7.1, SimpleInjector.Integration.Web.Mvc 2.7.1, SimpleSyndicate 1.0.23, SimpleSyndicate.Testing 1.0.13).
						</para>
					</listItem>
					<listItem>
						<para>
							Added ReSetup(IAppBuilder) and AnalyzeConfiguration to SimpleInjectorConfigurationBase.
						</para>
					</listItem>
					<listItem>
						<para>
							Minor XML comment fixes.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.10 was released on 24th January 2015.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (Newtonsoft.Json 6.0.8, SimpleInjector 2.7.0, SimpleInjector.Integration.Web 2.7.0, SimpleInjector.Integration.Web.Mvc 2.7.0).
						</para>
					</listItem>
					<listItem>
						<para>
							Fixed version in assembly not matching NuGet version.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.9 was released on 1st January 2015.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (AutoMapper 3.3.0, MvcCodeRouting 1.3.0, Newtonsoft.Json 6.0.7, SimpleInjector 2.6.1, SimpleInjector.Integration.Web 2.6.1, SimpleInjector.Integration.Web.Mvc 2.6.1, SimpleSyndicate 1.0.22, SimpleSyndicate.Testing 1.0.12).
						</para>
					</listItem>
					<listItem>
						<para>
							Updated the Simple Injector support to fully support ASP.Net Identity 2.1.
						</para>
					</listItem>
					<listItem>
						<para>
							Added confirmation message partial view.
						</para>
					</listItem>
					<listItem>
						<para>
							Added Web\Mvc\VersionHistoryActionFilterAttribute to automatically populate the view model with the current version.
						</para>
					</listItem>
					<listItem>
						<para>
							Moved editor templates to separate NuGet package.
						</para>
					</listItem>
					<listItem>
						<para>
							Renamed Auditing\AuditUser to Auditing\MvcAuditUser.
						</para>
					</listItem>
					<listItem>
						<para>
							Renamed Controllers\VersionHistory\LatestVersionViewModel to CurrentVersionViewModel.
						</para>
					</listItem>
					<listItem>
						<para>
							Improved XML comments.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.8 was released on 29th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Updated NuGet packages (Asp.Net Identity Core 2.1.0, Asp.Net Identity Framework 2.1.0, Asp.Net Identity Owin 2.1.0, SimpleSyndicate 1.0.17).
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.7 was released on 21st September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Changed PagingHelpers to not use Uris.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.6 was released on 19th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Fixed bug in AspNet\Identity\UserManagerFactory.Create.
						</para>
					</listItem>
					<listItem>
						<para>
							Fixed XML comment in <codeEntityReference>T:SimpleSyndicate.Mvc.Controllers.VersionHistory.VersionHistoryBuilder</codeEntityReference> Dispose method that should have referred to the repository passed to the constructor.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.5 was released on 18th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>
							Changed <codeEntityReference>T:SimpleSyndicate.Mvc.Web.Mvc.RedirectToRouteResultWithPaging</codeEntityReference> to derive from <codeEntityReference>T:System.Web.Mvc.RedirectToRouteResult</codeEntityReference>.
						</para>
					</listItem>
					<listItem>
						<para>
							Changed filename of <codeEntityReference>T:SimpleSyndicate.Mvc.Controllers.ConfirmationMessageActionPerformed</codeEntityReference> to match class name.
						</para>
					</listItem>
					<listItem>
						<para>
							Refactored Controllers.ConfirmationMessageViewModel.TempDataKeyName into <codeEntityReference>T:SimpleSyndicate.Mvc.Controllers.ConfirmationMessageImplementation</codeEntityReference>.
						</para>
					</listItem>
					<listItem>
						<para>
							Updated NuGet packages (Entity Framework 6.1.1, SimpleSyndicate 1.0.16, SimpleSyndicate.Testing 1.0.9).
						</para>
					</listItem>
					<listItem>
						<para>
							Changed non-solution documentation sources to reference components to prevent out of date issue.
						</para>
					</listItem>
					<listItem>
						<para>
							Added additional help file project to fix unresolved external references issue in build.
						</para>
					</listItem>
					<listItem>
						<para>
							Improved XML comments and implementations for classes that use IDisposable.
						</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.4 was released on 16th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Added parameterless constructor to Templates\SimpleInjectorConfiguration.</para>
					</listItem>
					<listItem>
						<para>Fixed example setup code in Templates\SimpleInjectorConfiguration.</para>
					</listItem>
					<listItem>
						<para>Made Controllers.ConfirmationMessageViewModel.TempDataKeyName public for testing purposes.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.3 was released on 15th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>No changes; re-pushed after symbols failed to publish.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.2 was released on 15th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Updated NuGet packages.</para>
					</listItem>
					<listItem>
						<para>Updated NuGet package release process to include symbols.</para>
					</listItem>
					<listItem>
						<para>Added generation of documentation to the release process.</para>
					</listItem>
					<listItem>
						<para>Removed unnecessary using statements.</para>
					</listItem>
					<listItem>
						<para>Satisfied FxCop rules.</para>
					</listItem>
				</list>
			</content>
		</section>

		<section>
			<title>Version 1.0.1 was not released.</title>
			<content>
				<para>Package failed to publish; re-pushed as 1.0.1.</para>
			</content>
		</section>

		<section>
			<title>Version 1.0.0 was released on 4th September 2014.</title>
			<content>
				<para>Changes in this release:</para>
				<list class="bullet">
					<listItem>
						<para>Initial release.</para>
					</listItem>
				</list>
			</content>
		</section>

		<relatedTopics>
			<link xlink:href="f50bb335-d9af-4fb1-8715-129f4be65848" />
		</relatedTopics>
	</developerConceptualDocument>
</topic>
