﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="f3e0dd41-c594-400a-a806-e94419d9e2cb" revisionNumber="1">
	<developerConceptualDocument xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5" xmlns:xlink="http://www.w3.org/1999/xlink">
		<introduction>
			<para>
				Using Entity Framework for data access is very common, and having an audit trail of all the changes that have been
				made to data is quite a common requirement. This package provides functionality to make it easy to add this to an
				application that uses Entity Framework.
			</para>
		</introduction>

		<section address="Section1">
			<content>
				<para>
					A relatively simple framework for auditing any changes made via Entity Framework is provided; it was designed for a
					code-first MVC application but should be adaptable to other types of applications.
				</para>
				<para>
					To show how this works, we'll use the standard <codeInline>ApplicationDbContext</codeInline> that a freshly created
					MVC application has, and we'll modify it so that any changes are audited.
				</para>
				<para>
					First, find the <codeInline>ApplicationDbContext</codeInline> class and add the following using statements:
				</para>
				<code language="cs" source="Content\GettingStarted\EntityFrameworkAuditing\Using.cs" />
				<para>
					Then, in the same file, add the auditing tables to it (they work just like normal code-first tables), e.g.:
				</para>
				<code language="cs" source="Content\GettingStarted\EntityFrameworkAuditing\ApplicationDbContextPartial.cs" />
				<para>
					With them added, all this is left is to override the standard <codeInline>SaveChanges</codeInline> method to call the
					SimpleSyndicate version that will save the changes, but also write out an audit trail of the changes as it does.
					Your <codeInline>ApplicationDbContext</codeInline> class will end up looking something like:
				</para>
				<code language="cs" source="Content\GettingStarted\EntityFrameworkAuditing\ApplicationDbContext.cs" />
				<para>
					Now whenever you call <codeInline>SaveChanges</codeInline>, it will use the SimpleSyndicate version that also saves the audit trail.
				</para>
				<para>
					You will notice that in the call to <codeInline>SaveChanges</codeInline> it passes in an <codeInline>MvcAuditUser</codeInline>
					instance -- this is because we want to record who has made the changes and so need to pass this information in.
				</para>
				<para>
					The <codeInline>MvcAuditUser</codeInline> instance is actually an implementation of an interface
					(<codeEntityReference>T:SimpleSyndicate.Auditing.IAuditUser</codeEntityReference>) -- <codeInline>SaveChanges</codeInline>
					takes an interface in so that how the user is determined can vary across applications. The audit trail doesn't actually require the
					user to be known (changes might be allowed by anonymous users for instance), so it doesn't matter if these details can't be determined
					(or aren't applicable).
				</para>
				<para>
					For MVC, the
					<externalLink>
						<linkText>SimpleSyndicate.Mvc package</linkText>
						<linkAlternateText>Open link in this window</linkAlternateText>
						<linkUri>https://www.nuget.org/packages/SimpleSyndicate.Mvc</linkUri>
						<linkTarget>_self</linkTarget>
					</externalLink>
					provides a suitable concrete implementation that provides the user id of the currently signed in user (see the <codeInline>MvcAuditUser</codeInline>
					class in <codeInline>SimpleSyndicate.Mvc.Auditing</codeInline>); this class is defined as:
				</para>
				<code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc\Auditing\MvcAuditUser.cs" />
			</content>
		</section>
		<relatedTopics>
			<externalLink>
				<linkText>SimpleSyndicate package</linkText>
				<linkAlternateText>Open link in this window</linkAlternateText>
				<linkUri>https://www.nuget.org/packages/SimpleSyndicate</linkUri>
				<linkTarget>_self</linkTarget>
			</externalLink>
			<externalLink>
				<linkText>SimpleSyndicate.Mvc package</linkText>
				<linkAlternateText>Open link in this window</linkAlternateText>
				<linkUri>https://www.nuget.org/packages/SimpleSyndicate.Mvc</linkUri>
				<linkTarget>_self</linkTarget>
			</externalLink>
		</relatedTopics>
	</developerConceptualDocument>
</topic>
