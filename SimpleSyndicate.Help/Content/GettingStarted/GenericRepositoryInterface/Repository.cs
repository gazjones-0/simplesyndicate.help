﻿using MyMvcApplication.Models;              // for ApplicationDbContext
using SimpleSyndicate.Mvc.Repositories;     // for the concrete generic repository implementation IdentityDbContextRepository

namespace MyMvcApplication.Repositories
{
	public class Repository<TEntity> : IdentityDbContextRepository<TEntity, ApplicationDbContext>
		where TEntity : class
	{
		public Repository(ApplicationDbContext context)
			: base(context)
		{
		}
	}
}
