﻿using System.Linq;
using SimpleSyndicate.Repositories;         // for the generic repository interface IRepository
using MyMvcApplication.Models;              // for the BlogPost entity model

namespace MyMvcApplication.Repositories
{
	public static class RepositoryBlogPostExtensions
	{
		// extension method that extends IRepository classes that work with BlogPost entities; we use the extension methods to
		// provide entity-specific functionality, in this example retrieving blog posts by a particular author
		public static IQueryable<BlogPost> ByAuthor(this IRepository<BlogPost> repository, string authorName)
		{
			return repository.All().Where(x => x.Author == authorName);
		}
	}
}
