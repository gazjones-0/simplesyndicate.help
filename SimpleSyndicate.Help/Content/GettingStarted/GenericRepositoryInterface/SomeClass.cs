﻿using System.Linq;
using SimpleSyndicate.Repositories;         // for the generic repository interface IRepository
using MyMvcApplication.Models;              // for the BlogPost entity model
using MyMvcApplication.Repositories;        // for the extension methods in RepositoryBlogPostExtensions

namespace MyMvcApplication.SomeClass
{
	public class SomeClass
	{
		private IRepository<BlogPost> BlogPostRepository { get; set; }
				
		// your preferred dependency injection library injects a Repository<BlogPost> concrete implementation
		public SomeClass(IRepository<BlogPost> blogPostRepository)
		{
			BlogPostRepository = blogPostRepository;
		}
					
		public void SomeMethod(string authorName)
		{
			// use the entension method to take advantage of the entity-specific functionality
			var blogPosts = BlogPostRepository.ByAuthor(authorName);
			// ...
		}
	}
}
