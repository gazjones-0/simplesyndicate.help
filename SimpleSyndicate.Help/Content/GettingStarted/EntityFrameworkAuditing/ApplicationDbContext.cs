﻿using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Auditing;         // for the auditing framework
using SimpleSyndicate.Models;           // for the auditing entity models
using SimpleSyndicate.Mvc.Models;       // for MvcAuditUser

// for this example, the standard IdentityUser has been used but a real MVC application would be better off
// using ApplicationUser from our SimpleSyndicate.Mvc package (see the SimpleSyndicate.Mvc.Models namespace)
public class ApplicationDbContext : IdentityDbContext<IdentityUser>
{
	public ApplicationDbContext()
		: base("DefaultConnection", throwIfV1Schema: false)
	{
	}

	// auditing tables
	public DbSet<AuditChangeSet> AuditChangeSets { get; set; }

	public DbSet<AuditObjectChange> AuditObjectChanges { get; set; }

	public DbSet<AuditPropertyChange> AuditPropertyChanges { get; set; }

	// ...
	// your application-specific tables here
	// ...

	// override the standard SaveChanges with our version which does the auditing
	public override int SaveChanges()
	{
		return Audit<ApplicationDbContext, IdentityUser>.SaveChanges(this, new MvcAuditUser());
	}
}
