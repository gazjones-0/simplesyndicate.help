﻿// for this example, the standard IdentityUser has been used but a real MVC application would be better off
// using ApplicationUser from our SimpleSyndicate.Mvc package (see the SimpleSyndicate.Mvc.Models namespace)
public class ApplicationDbContext : IdentityDbContext<IdentityUser>
{
	public ApplicationDbContext()
		: base("DefaultConnection", throwIfV1Schema: false)
	{
	}

	// auditing tables
	public DbSet<AuditChangeSet> AuditChangeSets { get; set; }

	public DbSet<AuditObjectChange> AuditObjectChanges { get; set; }

	public DbSet<AuditPropertyChange> AuditPropertyChanges { get; set; }
}
