﻿<?xml version="1.0" encoding="utf-8"?>
<topic id="9a2f5dee-9a85-4865-90e8-fa5c63d3ddd4" revisionNumber="1">
	<developerConceptualDocument xmlns="http://ddue.schemas.microsoft.com/authoring/2003/5" xmlns:xlink="http://www.w3.org/1999/xlink">
		<introduction>
			<para>
				Wanting to unit test code that uses a repository is very common -- usually, the class to be tested takes a repository
				interface in its constructor (i.e. via dependency injection), so in a unit test we'll pass in a repository that's
				suitable for unit testing with.
			</para>
			<para>
				We take the approach of having a <codeEntityReference>T:SimpleSyndicate.Repositories.IRepository`1</codeEntityReference>
				implementation that provides all the functionality of a repository, but without a permanent data store. This makes
				it easy to test functionality whilst also allowing easy setup of the repository with test data if necessary.
			</para>
		</introduction>

		<section address="Section1">
			<title>Test repository</title>
			<content>
				<para>
					The <codeEntityReference>T:SimpleSyndicate.Testing.Repositories.TestRepository`1</codeEntityReference> is a
					concrete <codeEntityReference>T:SimpleSyndicate.Repositories.IRepository`1</codeEntityReference> implementation
					that uses an in-memory data store, and uses no permanent storage.
				</para>
				<para>
					This class can be used to unit test functionality without a permanent store -- the general usage pattern is to
					instantiate an instance of the class for the desired entity, populate it (if necessary), then create an instance
					of the class to be tested, passing the test repository in as part of the constructor (i.e. in a real deployment
					the repository would be set via dependency injection).
				</para>
			</content>
		</section>

		<section address="Section2">
			<title>Not implemented repository</title>
			<content>
				<para>
					Within the <codeEntityReference>N:SimpleSyndicate.Testing.Repositories</codeEntityReference> namespace there is also a
					<codeEntityReference>T:SimpleSyndicate.Testing.Repositories.NotImplementedRepository`1</codeEntityReference> that is
					a concrete <codeEntityReference>T:SimpleSyndicate.Repositories.IRepository`1</codeEntityReference> implementation that
					throws a <codeEntityReference>T:System.NotImplementedException</codeEntityReference> on every method. This is useful
					for unit testing exception handling paths.
				</para>
			</content>
		</section>

		<relatedTopics>
			<externalLink>
				<linkText>SimpleSyndicate package</linkText>
				<linkAlternateText>Open link in this window</linkAlternateText>
				<linkUri>https://www.nuget.org/packages/SimpleSyndicate</linkUri>
				<linkTarget>_self</linkTarget>
			</externalLink>
			<externalLink>
				<linkText>SimpleSyndicate.Mvc package</linkText>
				<linkAlternateText>Open link in this window</linkAlternateText>
				<linkUri>https://www.nuget.org/packages/SimpleSyndicate.Mvc</linkUri>
				<linkTarget>_self</linkTarget>
			</externalLink>
		</relatedTopics>

	</developerConceptualDocument>
</topic>
