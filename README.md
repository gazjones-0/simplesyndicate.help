# README #

SimpleSyndicate.Help project

View the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### What is this repository for? ###

* Project for building help documentation for the SimpleSyndicate packages.

### How do I get set up? ###

* Not really applicable -- this project only exists to build the help documentation.

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.help/issues?status=new&status=open
